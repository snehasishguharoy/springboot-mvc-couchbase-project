package com.example.couchbase.service;

import com.example.couchbase.entity.Project;
import com.example.couchbase.entity.Task;
import com.example.couchbase.repo.ProjectRepo;
import com.example.couchbase.repo.TaskRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepo projectRepo;
	@Autowired
	private TaskRepo taskRepo;

	@Override
	public Project createProject(Project project) {
		return projectRepo.save(project);
	}

	@Override
	public Task createTask(Task t) {
		return taskRepo.save(t);
	}

	@Override
	public List<Project> findAll() {
		return projectRepo.findAll();
	}

	@Override
	public Project findById(String id) {
		return projectRepo.findById(id).orElse(null);
	}

	@Override
	public void deleteProject(String projectId) {
		projectRepo.deleteById(projectId);

	}
}
