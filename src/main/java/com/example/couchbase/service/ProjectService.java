package com.example.couchbase.service;

import com.example.couchbase.entity.Project;
import com.example.couchbase.entity.Task;

import java.util.List;

public interface ProjectService {

	Project createProject(Project project);

	Task createTask(Task t);

	List<Project> findAll();

	Project findById(String id);

	void deleteProject(String projectId);

}
