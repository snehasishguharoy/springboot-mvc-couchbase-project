package com.example.couchbase.cntlr;

import com.example.couchbase.entity.Project;
import com.example.couchbase.entity.Task;
import com.example.couchbase.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/project")
public class ProjectCntlr {

	@Autowired
	private ProjectService projectService;

	@PostMapping("/save")
	public Project createProject(@RequestBody Project project) {
		return projectService.createProject(project);
	}

	@PostMapping("/saveTask")
	public Task createTask(@RequestBody Task task) {
		return projectService.createTask(task);
	}

	@GetMapping("/find/all")
	public List<Project> findAll() {
		return projectService.findAll();
	}

	@GetMapping("/find/{id}")
	public Project findById(@PathVariable("id") String id) {
		return projectService.findById(id);
	}

	@DeleteMapping("/delete/{projectId}")
	public void deleteProject(@PathVariable("projectId") String projectId) {
		projectService.deleteProject(projectId);
	}

}
