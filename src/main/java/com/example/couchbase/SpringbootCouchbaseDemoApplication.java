package com.example.couchbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootCouchbaseDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCouchbaseDemoApplication.class, args);
	}

}
