package com.example.couchbase.repo;

import com.example.couchbase.entity.Task;
import org.springframework.data.couchbase.repository.CouchbaseRepository;

public interface TaskRepo extends CouchbaseRepository<Task,String> {
}
