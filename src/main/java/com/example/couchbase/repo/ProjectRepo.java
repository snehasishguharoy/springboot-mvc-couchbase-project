package com.example.couchbase.repo;

import com.example.couchbase.entity.Project;
import org.springframework.data.couchbase.repository.CouchbaseRepository;

public interface ProjectRepo extends CouchbaseRepository<Project,String> {



}
